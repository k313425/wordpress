<?php get_header(); ?>
<div id="index">
	<h2>搜索结果</h2>
	<ol>
		<?php if(have_posts()): while (have_posts()) : the_post();?>
		<li><a href="<?php the_permalink();?>"><span class="data"><?php the_time('Y.m.d(l)');?></span> --- <span><?php the_title();?></span></a></li>
		<?php endwhile; endif;?>
	</ol>
	<div class="posts-nav">
		<?php echo paginate_links(array(
			'show_all'     => False,
			'end_size'     => 1,
			'mid_size'     => 2,
			'prev_next'    => True,
			'prev_text'    => __(' Prev '),
			'next_text'    => __(' Next '))
		);?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>