<div id="side">
	
	<div class="free_box">
		<div class="title">
			<a href="<?php bloginfo('url');?>/wp/"><img src="<?php bloginfo('template_url');?>/img/common/icon08.png" width="0" alt=""><span>WP</span></a>
		</div>
		<ul class="cat_box">
			<li><a href="<?php bloginfo('url');?>/wp/">wp(<?php
			echo get_category_by_slug('wp')->count;
			?>)</a></li>
		</ul>
	</div>
	
	<?php if ( is_active_sidebar( 'side_box' ) ) : ?>
		<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
			<?php dynamic_sidebar( 'side_box' ); ?>
		</div><!-- #primary-sidebar -->
	<?php endif; ?>
	
</div><!--/sub-->