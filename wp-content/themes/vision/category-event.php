<?php get_header(); ?>

<div id="index">
	<ol>
		<?php if(have_posts()): while (have_posts()) : the_post();
		/*the_content();*/?>
		<li><a href="<?php the_permalink();?>"><span><?php the_title();?></span></a></li>
		<?php endwhile; endif;?>
	</ol>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>