<?php get_header(); ?>

<div id="index">

	<?php if(have_posts()): while (have_posts()) : the_post();?>
	
	<div class="newsBox comBox">
		
		<h2 class="h3Ttl"><font class="youmin"><?php the_title();?></font><span><?php the_time('Y.m.d');?></span></h2>
		<div class="wpTxt"><?php the_content();?></div>
		
		<div class="pho"><?php get_the_thumbnail();?></div><!-- 栏目图片 -->
		<?php endwhile; endif;?>
		<?php
		/* Start the Loop */
		while ( have_posts() ) :
			the_post();
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			the_post_navigation(
				array(
					'prev_text' => '上一篇',
					'next_text' => '下一篇',
				)
			);
		endwhile; // End of the loop.
		?>
		<h4>相关文章</h4>
		<ol id="cat_related"><?php
		global $post;
		$cats = wp_get_post_categories($post->ID);
		if ($cats) {
			$args = array(
				  'category__in' => array( $cats[0] ),
				  'post__not_in' => array( $post->ID ),
				  'showposts' => 5,
				  'caller_get_posts' => 1
			  );
		  query_posts($args);
		
		  if (have_posts()) {
			while (have_posts()) {
			  the_post(); update_post_caches($posts); ?>
		  <li><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
		<?php
			}
		  } 
		  else {
			echo '<li>暂无相关文章</li>';
		  }
		  wp_reset_query(); 
		}
		else {
		  echo '<li>暂无相关文章</li>';
		}
		?>
		</ul>
		<div class="btmLinks clearfix">
			<div class="lBox"><a href="./">返回首页</a></div>
			<div class="rBox">
				<a href="#"><img src="<?php bloginfo('template_url');?>/img/common/icon_f.png" width="20" alt=""></a>
				<a href="#"><img src="<?php bloginfo('template_url');?>/img/common/icon_t.png" width="20" alt=""></a>
			</div>
		</div>
	</div>
	<div class="posts-nav">
		<?php echo paginate_links(array(
			'show_all'     => False,
			'end_size'     => 1,
			'mid_size'     => 2,
			'prev_next'    => True,
			'prev_text'    => __(' Prev '),
			'next_text'    => __(' Next '))
		);?>
	</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>