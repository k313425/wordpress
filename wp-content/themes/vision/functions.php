<?php
function remove_wp_open_sans() {
	wp_deregister_style( 'open-sans' );
	wp_register_style( 'open-sans', false );
}
add_action('wp_enqueue_scripts', 'remove_wp_open_sans');
add_action('admin_enqueue_scripts', 'remove_wp_open_sans');

add_theme_support( 'post-thumbnails' );
//add_image_size('size-news',240,175,true);

/**
* get page name
**/
function getPageName(){
	if(is_page()){
		$pageId = get_the_ID();
		$curPage = get_page($pageId);
		$curPageParent = $curPage->post_parent;
		if($curPageParent == 0){
			$pname = $curPage->post_name;
		}else{
			$pname = get_page(get_top_parent_page_id())->post_name;
		}
	}
	return $pname;
}

function get_top_parent_page_id() {
    global $post;
    $ancestors = $post->ancestors;
    if ($ancestors) {
        return end($ancestors);
    } else {
        return $post->ID;
    }
}

//修改后台显示更新的代码

add_filter('pre_site_transient_update_core',    create_function('$a', "return null;")); // 关闭核心提示
add_filter('pre_site_transient_update_plugins', create_function('$a', "return null;")); // 关闭插件提示
add_filter('pre_site_transient_update_themes',  create_function('$a', "return null;")); // 关闭主题提示
remove_action('admin_init', '_maybe_update_plugins'); // 禁止 WordPress 更新插件
remove_action('admin_init', '_maybe_update_core');    // 禁止 WordPress 检查更新
remove_action('admin_init', '_maybe_update_themes');  // 禁止 WordPress 更新主题


//解决WordPress最大上传文件大小限制修改
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );
 
// 添加HTML按钮自定义标签名称
function appthemes_add_quicktags() {
?>
	<script>
		QTags.addButton( 'pre', 'pre', '<pre class="php">\n<xmp>', '\n</xmp></pre>\n' );
	</script>
<?php
}
add_action('admin_print_footer_scripts', 'appthemes_add_quicktags' );

//缩略图获取
function get_the_thumbnail() {
	global $post;
	if (has_post_thumbnail ()) {
		//如果存在缩略图读取之
		echo '<a href="' . get_permalink () . '" class="pic" title="'.trim ( strip_tags ( $post->post_title ) ).'">';
		$domsxe = simplexml_load_string ( get_the_post_thumbnail () );
		$thumbnailsrc = $domsxe->attributes()->src;
		echo '<img src="' . $thumbnailsrc . '" alt="' . trim ( strip_tags ( $post->post_title ) ) . '">';
		echo '</a>';
	} else {
		//读取文章第一张图片为缩略图
		$content = $post->post_content;
		preg_match_all ( '/<img.*?(?: |\\t|\\r|\\n)?src=[\'"]?(.+?)[\'"]?(?:(?: |\\t|\\r|\\n)+.*?)?>/sim', $content, $strResult, PREG_PATTERN_ORDER );
		$n = count ( $strResult [1] );
		if ($n > 0) {
			//文章第一张图片
			echo '<a href="' . get_permalink () . '" class="pic" title="'.trim ( strip_tags ( $post->post_title ) ).'"><img src="' . $strResult [1] [0] . '"></a>';
		} else {
			//如果文章没有图片则读取默认图片
			//echo '<a href="' . get_permalink () . '" class="pic" title="'.trim ( strip_tags ( $post->post_title ) ).'"><img src="' . get_bloginfo ( 'template_url' ) . '/img/common/logo.png"></a>';
		}
	}
}

/**
 * Register our sidebars and widgetized areas.
 * wp 小工具 插件
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => '侧边栏',
		'id'            => 'side_box',
		'before_widget' => '<div class="free_box">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="title"><span>',
		'after_title'   => '</span></div>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


//回复评论
function custom_comment_reply($content) {
	if ( is_singular() && get_option( 'thread_comments' ) )
	wp_enqueue_script( 'comment-reply' );
	
	$content = str_replace('回复', '回复此评论', $content);
	return $content;
}
add_filter('comment_reply_link', 'custom_comment_reply');


/*禁用未登录的用户*/
// add_filter( 'rest_api_init', 'rest_only_for_authorized_users', 99 );
// function rest_only_for_authorized_users($wp_rest_server){
//     if ( !is_user_logged_in() ) {
//         wp_die('非法操作! <a href="/wp-login.php">请先登陆</a>');
//     }
// }
// 

