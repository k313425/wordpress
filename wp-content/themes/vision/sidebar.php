<div id="side">
	
	
	<div class="free_box">
		<div class="title">
			<a href="<?php bloginfo('url');?>/tag-cloud/"><img src="<?php bloginfo('template_url');?>/img/common/icon04.png" alt=""><span>标签云</span></a>
		</div>
		<ul class="cat_box">
			<?php $tags = get_tags( array( 'orderby' => '', 'order' => 'ASC' ) );
			$html = '';
			foreach ( $tags as $tag ) {
				$tag_link = get_tag_link( $tag->term_id );
						
				$html .= "<li><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
				$html .= "{$tag->name}($tag->count)</a></li>";
			}
			echo $html;?>
		</ul>
	</div>
	
</div><!--/sub-->