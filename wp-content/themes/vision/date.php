<?php get_header(); ?>
<div id="index">
	<div class="newsBox comBox">
	<h2><?php the_time('Y.m.d(l)'); ?></h2>

	<dl class="news">
		<?php if(have_posts()): while (have_posts()) : the_post();
		/*the_content();*/?>
		<dt><?php the_time('Y.m.d');?></dt>
		<dd><a href="<?php the_permalink();?>"><?php the_title();?></a></dd>
		<?php endwhile; endif;?>
	</dl>
	
	
	<div class="posts-nav">
		<?php echo paginate_links(array(
			'show_all'     => False,
			'end_size'     => 1,
			'mid_size'     => 2,
			'prev_next'    => True,
			'prev_text'    => __(' Prev '),
			'next_text'    => __(' Next '))
		);?>
	</div>
</div>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>