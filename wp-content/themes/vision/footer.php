    </div><!--/contents-->

    <div id="footer">

        <div class="inner pc clearfix">
			<div class="page_top"><a href="#wrapper"><img src="<?php bloginfo('template_url');?>/img/common/page_top.png" alt="TOP"></a></div>
	
			<div class="category_box">
				<div class="ttl"><a href=""><img src="<?php bloginfo('template_url');?>/img/common/icon10.png" alt="">HTML</a></div>
				<ul class="clearfix">
					<li>
						<a href="">分类1</a>
					</li><li>
						<a href="">分类2</a>
					</li><li>
						<a href="">分类3</a>
					</li><li>
						<a href="">分类4</a>
					</li>
				</ul>
			</div>
			<div class="category_box">
				<div class="ttl"><a href=""><img src="<?php bloginfo('template_url');?>/img/common/icon11.png" alt="">CSS</a></div>
				<ul class="clearfix">
					<li>
						<a href="">分类1</a>
					</li><li>
						<a href="">分类2</a>
					</li><li>
						<a href="">分类3</a>
					</li><li>
						<a href="">分类4</a>
					</li>
				</ul>
			</div>
			<div class="category_box">
				<div class="ttl"><a href=""><img src="<?php bloginfo('template_url');?>/img/common/icon12.png" alt="">JS</a></div>
				<ul class="clearfix">
					<li>
						<a href="">分类1</a>
					</li><li>
						<a href="">分类2</a>
					</li><li>
						<a href="">分类3</a>
					</li><li>
						<a href="">分类4</a>
					</li>
				</ul>
			</div>
			<div class="category_box">
				<div class="ttl"><a href=""><img src="<?php bloginfo('template_url');?>/img/common/icon13.png" alt="">PHP</a></div>
				<ul class="clearfix">
					<li>
						<a href="">分类1</a>
					</li><li>
						<a href="">分类2</a>
					</li><li>
						<a href="">分类3</a>
					</li><li>
						<a href="">分类4</a>
					</li>
				</ul>
			</div>
			<div class="category_box">
				<div class="ttl"><a href=""><img src="<?php bloginfo('template_url');?>/img/common/icon14.png" alt="">WP</a></div>
				<ul class="clearfix">
					<li>
						<a href="">分类1</a>
					</li><li>
						<a href="">分类2</a>
					</li><li>
						<a href="">分类3</a>
					</li><li>
						<a href="">分类4</a>
					</li>
				</ul>
			</div>
        </div>
		
		<div class="inner sp">
			<h2 class="h2Ttl">类别一览</h2>

			<div class="page_top"><a href="#wrapper"><img src="<?php bloginfo('template_url');?>/img/common/page_top.png" alt="TOP"></a></div>
	
			<div class="category_box">
				<div class="ttl"><a href="">类别1</a></div>
				<ul class="clearfix">
					<li>
						<a href="">分类1</a>
					</li><li>
						<a href="">分类2</a>
					</li><li>
						<a href="">分类3</a>
					</li><li>
						<a href="">分类4</a>
					</li>
				</ul>
			</div>
			<div class="category_box">
				<div class="ttl"><a href="">类别2</a></div>
				<ul class="clearfix">
					<li>
						<a href="">分类1</a>
					</li><li>
						<a href="">分类2</a>
					</li><li>
						<a href="">分类3</a>
					</li><li>
						<a href="">分类4</a>
					</li>
				</ul>
			</div>
        </div>
		
		<div class="link_box sp">
			<p class="ttl">类别菜单</p>
			<ul>
				<li><a href="#">分类1</a></li>
				<li><a href="#">分类2</a></li>
				<li><a href="#">分类3</a></li>
				<li><a href="#">分类4</a></li>
				<li><a href="#">分类5</a></li>
			</ul>
		</div>

		<p class="copyright">Copyright (C) <a href="<?php bloginfo('url');?>">wp网站</a> All Rights Reserved.</p>

    </div><!-- /footer-->
	
</div><!-- /wrapper-->

<?php wp_footer(); ?>
</body>
</html>