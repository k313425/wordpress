<?php get_header(); ?>
<style>
	.banner-line {display: flex;align-items: center;text-align: center;position: fixed;top: 0; left: 0; right: 0;bottom: 0;width: 750px; height: 750px;z-index: -1;margin: auto;transform: scale(0.5);}.banner-line > i {position: absolute;top: 0;right: 0;bottom: 0;left: 0;display: block;margin: auto;}.banner-line__circle01 {-webkit-animation: ani_line 40s linear infinite;animation: ani_line 40s linear infinite;}.banner-line__circle02 {animation: ani_line 30s linear infinite reverse;}.banner-line__circle01 {background-image:url(https://www.xunlei.com/v2018/dist/spr_bannerPathway.png?h=2a666c);background-position:-2436px 0px;background-repeat:no-repeat;width: 719px;height: 719px;}.banner-line__circle02 {background-image:url(https://www.xunlei.com/v2018/dist/spr_bannerPathway.png?h=2a666c);background-position:-2436px -724px;background-repeat:no-repeat;width: 648px;height: 674px;}@-webkit-keyframes ani_line {from {-webkit-transform: none;transform: none;}to {-webkit-transform: rotate(360deg);transform: rotate(360deg);}}@keyframes ani_line {from {-webkit-transform: none;transform: none;}to {-webkit-transform: rotate(360deg);transform: rotate(360deg);}}#canvas {margin: auto;}
</style>
<script src="<?php bloginfo('template_url');?>/js/jquery.countdown.js"></script>
<script src="<?php bloginfo('template_url');?>/js/canvas.js"></script>
<script>
window.jQuery(function ($) {
	"use strict";
	$('time').countDown({
		with_separators: false
	});
});
</script>
<div id="index">
	<div class="banner-line"> <i class="banner-line__circle01"></i> <i class="banner-line__circle02"></i> 
		<canvas id="canvas" width="500" height="500"></canvas>
	</div>
	
	<div id="content" style="display: none;">
		<!-- Valid global date and time string -->
		<!--<div><time>2018-09-02T04:00:00+0100</time></div><!-- Paris (winter) -->
		<!--<div><time>2018-09-02T04:00:00+0000</time></div><!-- UTC -->
		<div><time>2019-09-02T04:00:00+0800</time></div><!-- China -->
	</div>
	<h2>最近15篇文章</h2>
	<?php wp_reset_query(); ?>
	<ol><?php query_posts("v_sortby=views&orderby=date&v_orderby=desc&showposts=15"); ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<li><a class="<?php
			$t1=$post->post_date;
			$t2=date("Y-m-d H:i:s");
			$diff=(strtotime($t2)-strtotime($t1))/3600;
			if($diff<12){echo 'new';}
			?>" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><span class="data"><?php the_time('Y.m.d');?></span> --- <?php the_title() ?></a></li>
		<?php endwhile; endif;?>
	</ol>
	<?php wp_reset_query(); ?>
	<h2>最近七天的文章</h2>
	<?php wp_reset_query(); ?>
	<?php function mostweek($where = '') {
		//获取最近七天的文章
		$where .= " AND post_date > '" . date('Y-m-d', strtotime('-7 days')) . "'";
		return $where;
	}
	add_filter('posts_where', 'mostweek');
	query_posts($query_string) ?>
	<ol>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<li><a class="<?php
			$t1=$post->post_date;
			$t2=date("Y-m-d H:i:s");
			$diff=(strtotime($t2)-strtotime($t1))/3600;
			if($diff<12){echo 'new';}
			?>" href="<?php the_permalink() ?>" title="<?php the_title() ?>"><span class="data"><?php the_time('Y.m.d');?></span> --- <?php the_title() ?></a></li>
		<?php endwhile; endif;?>
	</ol>
	
	<div class="posts-nav">
		<?php echo paginate_links(array(
			'show_all'     => False,
			'end_size'     => 1,
			'mid_size'     => 2,
			'prev_next'    => True,
			'prev_text'    => __(' Prev '),
			'next_text'    => __(' Next '))
		);?>
	</div>
	<?php wp_reset_query(); ?>
	<div class="cur">
		
	</div>
	<div class="pages">
		
	</div>
	<script>
		jQuery(document).ready(function($) {
			$.ajax({ //发起ajax请求
				url: "https://www.yalayi.com/gallery/", //请求的地址就是下一页的链接
				type: "get",
				
				error: function(request) {
					console.log('error');
				},
				success: function(data) {
					$('.pages').append($(data).find("a"));
					$('.cur').append($(data).find(".img-box"));
					console.log('success');
				}
			})
			.then(() => {
				var length = $('.pages a').length;
				for(var i = 0;i < length;i++){
					var href = $('.pages a:eq('+ i +')').attr("href");
					console.log(href);
					$.ajax({ //发起ajax请求
						url: 'https://www.yalayi.com' + href, //请求的地址就是下一页的链接
						type: "get",

						error: function(request) {
							console.log('error');
						},
						success: function(data) {
							$('.cur').append($(data).find(".img-box"));
							console.log('success');
						}
					});
				}
			});
		});
	</script>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>