<?php get_header(); ?>
<style>
#page_path {
	display: none !important;
}
</style>
<div id="index">
	<h2><?php single_cat_title(); ?></h2>
	<ol>
		<?php if(have_posts()): while (have_posts()) : the_post();
		/*the_content();*/?>
		<li><a href="<?php the_permalink();?>"><span class="data"><?php the_time('Y.m.d(l)');?></span>  <span><?php the_title();?></span></a></li>
		<?php endwhile; endif;?>
	</ol>
	<div class="posts-nav">
		<?php echo paginate_links();?>
	</div>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>