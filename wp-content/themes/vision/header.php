<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<title><?php
/*
 * Print the <title> tag based on what is being viewed.
 */
global $page, $paged;
wp_title( '|', true, 'right' );
// Add the blog name.
bloginfo( 'name' );
// Add the blog description for the home/front page.
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
	echo " | $site_description";
// Add a page number if necessary:
if ( $paged >= 2 || $page >= 2 )
	echo ' | ' . sprintf( __( 'Page %s', '' ), max( $paged, $page ) );
?></title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/common.css">
<script src="<?php bloginfo('template_url');?>/js/jquery.js"></script>
<script src="<?php bloginfo('template_url');?>/js/lang.js"></script>
<script src="<?php bloginfo('template_url');?>/js/style.js"></script>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url');?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<body>

<div id="wrapper">

	<div class="bg sp">bg</div>
    <div id="header">
        <div class="lead_area">
					<p>于Σ(っ °Д °;)っ朕的第一个全端网站
						<span class="lang"><a href="javascript:zh_tran('t');" id="zh_click_t">繁</a> /
						<a href="javascript:zh_tran('s');" id="zh_click_s">简</a> /
						<a href="/app/">app</a></span>
					</p>
        </div>
        <div class="inner site_area clearfix">
			<div class="hBox clearfix">
				<div class="logo"><a href="<?php bloginfo('url');?>"><img src="<?php bloginfo('template_url');?>/img/common/logo.png" alt=""></a></div>
				<h1 class="site_ttl youmin"><span></span></h1>
			</div>
			<div class="menu sp"><span class="top"></span><span class="middle"></span><span class="bottom"></span></div>
			<div id="g_navi">
				<div class="inner youmin">
				<ul>
					<li><a href="<?php bloginfo('url');?>/html/">HTML(<?php echo get_category_by_slug('html')->count; ?>)</a></li>
					<li><a href="<?php bloginfo('url');?>/css/">CSS(<?php echo get_category_by_slug('css')->count; ?>)</a></li>
					<li><a href="<?php bloginfo('url');?>/js/">JS(<?php echo get_category_by_slug('js')->count; ?>)</a></li>
					<li><a href="<?php bloginfo('url');?>/php/">PHP(<?php echo get_category_by_slug('php')->count; ?>)</a></li>
					<li><a href="<?php bloginfo('url');?>/wp/">WP(<?php echo get_category_by_slug('wp')->count; ?>)</a></li>
				</ul>
				</div>
			</div><!--/g_navi-->
        </div>
		<div class="menu_box">
			<p class="ttl"><a href="<?php bloginfo('url');?>/html/">HTML</a></p>
			<ul>
				<li><a href="#">子分类</a></li>
				<li><a href="#">子分类</a></li>
			</ul>
			<p class="ttl"><a href="<?php bloginfo('url');?>/css/">CSS</a></p>
			<ul>
				<li><a href="#">子分类</a></li>
				<li><a href="#">子分类</a></li>
			</ul>
			<p class="ttl"><a href="<?php bloginfo('url');?>/js/">JS</a></p>
			<ul>
				<li><a href="#">子分类</a></li>
				<li><a href="#">子分类</a></li>
			</ul>
			<p class="ttl"><a href="<?php bloginfo('url');?>/php/">PHP</a></p>
			<ul>
				<li><a href="#">子分类</a></li>
				<li><a href="#">子分类</a></li>
			</ul>
			<p class="ttl"><a href="<?php bloginfo('url');?>/wp/">WP</a></p>
			<ul>
				<li><a href="#">子分类</a></li>
				<li><a href="#">子分类</a></li>
			</ul>
			
			<div class="close"><span>×</span>关闭</div>
		</div>
    </div>
        
	
	<?php if(is_page()||is_single()){ ?>
	<ul id="page_path">
		<li><a href="<?php bloginfo('url');?>">HOME</a>&gt;</li>
		<li><?php the_title();?></li>
	</ul><!--/page_path-->     
    <?php } ?>
    <div id="contents" class="clearfix">
