// if(((navigator.userAgent.indexOf('iPhone') > 0) || (navigator.userAgent.indexOf('Android') > 0) && (navigator.userAgent.indexOf('Mobile') > 0) && (navigator.userAgent.indexOf('SC-01C') == -1))){
// document.write('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">');
// }	
(function (doc, win) {
	var docEl = doc.documentElement,
	isIOS = navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
	dpr = isIOS ? Math.min(win.devicePixelRatio, 3) : 1,
	dpr = window.top === window.self ? dpr : 1, //被iframe引用时，禁止缩放
	dpr = 1,
	scale = 1 / dpr,
	resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
	docEl.dataset.dpr = dpr
	var metaEl = doc.createElement('meta')
	metaEl.name = 'viewport'
	metaEl.content = 'initial-scale=' + scale + ',maximum-scale=' + scale + ', minimum-scale=' + scale
	docEl.firstElementChild.appendChild(metaEl)
	var recalc = function () {
	var width = docEl.clientWidth
	if (width / dpr > 750) {
		width = 750 * dpr
	}
	// 乘以100，px : rem = 100 : 1
	docEl.style.fontSize = 100 * (width / 750) + 'px'
	}
	recalc()
	if (!doc.addEventListener) return
	win.addEventListener(resizeEvt, recalc, false)
})(document, window)

//page-scroller
$(function(){
	$(document).on('click','a[href*=#]:not([href=#])',function(){
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
		&& location.hostname == this.hostname) {
			var $target = $(this.hash);
			$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			if ($target.length) {
				var targetOffset = $target.offset().top
				$('html,body').animate({scrollTop: targetOffset}, 1000);
				return false;
			}
		}
	});
});


$(function(){
		$('.menu').click(function(){
		$(this).toggleClass('active');
		$('.menu_box').slideToggle();
		$('.bg').slideToggle();
	});
	
	$('.close').click(function(){
		$('.menu').removeClass('active');
		$('.menu_box').slideToggle();
		$('.bg').slideToggle();
	});
	
	if($('#side ul').length){
		$('#side ul').addClass('cat_box');
	}
	
	// 手风琴
// 	$('.table-view-cell.collapse > a').click(function(){
// 		$(this).next('.collapse-content').toggle();
// 		$(this).parent('.table-view-cell.collapse').toggleClass('active');
// 		return false;
// 	});
	
	// 滑动
// 	$('.control-item').click(function(){
// 		var ind=$(this).index();
// 		$(this).addClass('active').siblings().removeClass('active');
// 		$('.control-content').hide();		
// 		$('.control-content:eq('+ind+')').show();
// 		return false;
// 	});

	
	var minH = $(window).height() - $("#gHeader").height() - $("#gFooter").height();
	$('#main').css("min-height",minH);
	
	
	//图片hover
	if ($(window).width() > 767) {
		$('.hover').hover(function(){
			$(this).find('img').each(function(){
				var src = $(this).attr('src');
				src = src.match('_out') ? src.replace('_out', '_over') : src.replace('_over', '_out');
				$(this).attr('src',src);
			});
		});
	} else{
		$('.hover').find('img').each(function(){
			var src = $(this).attr('src');
			src = window.screen.width < 768 ? src.replace('_over', '_out') : src.replace('_out', '_over');
			$(this).attr('src',src);
		});
	}
	
	// 二维码
// 	$('.qrcode').qrcode({
// 		render: 'canvas', //也可以替换为table
// 		width: 100,
// 		height: 100,
// 		text: this.location.href
// 	});
});
