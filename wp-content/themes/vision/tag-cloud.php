<?php /*Template Name: Tags Cloud*/ ?>
<?php get_header(); ?>
<div id="index">
	<div><?php wp_tag_cloud();?></div>
	
	<br>

	<?php $tags = get_tags( array( 'orderby' => '', 'order' => 'ASC' ) );
	$html = '<ol>';
	foreach ( $tags as $tag ) {
		$tag_link = get_tag_link( $tag->term_id );
				
		$html .= "<li><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
		$html .= "{$tag->name}($tag->count)</a></li>";
	}
	$html .= '</ol>';
	echo $html;?>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>