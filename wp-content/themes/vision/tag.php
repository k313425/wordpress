<?php get_header(); ?>
<div id="index">
	<div class="newsBox comBox">
	<h2><?php the_time('Y.m.d(l)'); ?></h2>

	<dl class="news">
		<?php if(have_posts()): while (have_posts()) : the_post();
		/*the_content();*/?>
		<dt><?php the_time('Y.m.d');?></dt>
		<dd><a href="<?php the_permalink();?>"><?php the_title();?></a></dd>
		<?php endwhile; endif;?>
	</dl>
	
	<div id="pagination">
		<?php next_posts_link(__('点击查看更多')); ?>
	</div>
</div>
<script>
jQuery(document).ready(function($) {
	$('#pagination a').addClass('btn-out');
	
	function scrollmore() {
		$this = $('#pagination a');
		$this.addClass('loading').text("正在努力加载");
		var href = $this.attr("href");
		if (href) {
			console.log(href);
			$.ajax({ //发起ajax请求
				url: href, //请求的地址就是下一页的链接
				type: "get", //请求类型是get
				error: function(request) {
					console.log('error');
				},
				success: function(data) {
					$this.removeClass('loading').text("点击查看更多");
					var $res = $(data).find(".news").html();
					$('.news').append($res);
					var newhref = $(data).find("#pagination a").attr("href");
					if (newhref) {
						$("#pagination a").attr("href", newhref);
					} else {
						$("#pagination a").remove();
						$("#pagination").text("没有下一页了");
					}
				}
			});
		} else {
			return false;
		}
	}
	var pageW = 0;
	var pageH = 1;
	var scrollT = 2;
	var scrollHandler = function () {
		pageH = $('#pagination').offset().top - $(window).height();
		scrollT = $(window).scrollTop(); //滚动条top 
		if ((scrollT > pageH) && (pageW != pageH)){
			scrollmore();
			pageW = $('#pagination').offset().top - $(window).height();//防止多次加载
		}
	}
	$('#pagination a').click(function(){
		scrollmore();
		return false;
	});
	//定义鼠标滚动事件
	$(window).scroll(scrollHandler);
});
</script>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>